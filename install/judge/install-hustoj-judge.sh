#!/bin/bash

NOWPATH=`pwd`
/usr/sbin/useradd -m -u 1536 judge
cd /home/judge/
apt-get update
apt-get install -y make flex g++ clang libmysqlclient-dev libmysql++-dev  memcached  mysql-server  fp-compiler  openjdk-8-jdk

CPU=`grep "cpu cores" /proc/cpuinfo |head -1|awk '{print $4}'`
COMPENSATION=`grep 'mips' /proc/cpuinfo|head -1|awk -F: '{printf("%.2f",$2/5000)}'`
mkdir etc data log
chmod -R 777 data
cp $NOWPATH/install/java0.policy  /home/judge/etc
cp $NOWPATH/install/judge.conf  /home/judge/etc

mkdir run0 run1 run2 run3
chown judge run0 run1 run2 run3


sed -i "s/OJ_RUNNING=1/OJ_RUNNING=$CPU/g" etc/judge.conf
sed -i "s/OJ_CPU_COMPENSATION=1.0/OJ_CPU_COMPENSATION=$COMPENSATION/g" etc/judge.conf

cd $NOWPATH/core
chmod +x ./make.sh
./make.sh
if grep "/usr/bin/judged" /etc/rc.local ; then
	echo "auto start judged added!"
else
	sed -i "s/exit 0//g" /etc/rc.local
	echo "/usr/bin/judged" >> /etc/rc.local
	echo "exit 0" >> /etc/rc.local
	
fi
mv $NOWPATH/install/judged /etc/init.d/
chmod +x /etc/init.d/judged
update-rc.d judged defaults
echo '判题机部署完成.修改配置文件,开启服务'
