ALTER TABLE `contest_problem` ADD `alias` VARCHAR(200) NOT NULL AFTER `num`;

ALTER TABLE `contest_problem` CHANGE `title` `problem_title` CHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';

TRUNCATE contest;
ALTER TABLE contest AUTO_INCREMENT=1;


-- 创建 比赛讨论区的表;
CREATE TABLE contest_discuss(
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '自增主键',
    `pre` int(11) NOT NULL DEFAULT -1 COMMENT '父亲节点, -1 代表根节点',
    `cid` int(11) NOT null COMMENT '比赛ID',
    `num` int(11) NOT null DEFAULT -1 COMMENT '比赛的第几题 -1 代表public讨论',
    `user_id` varchar(48) NOT null COMMENT '发布者的用户ID',
    `in_time` int(11) NOT null COMMENT '发布的时间戳',
    `content` text NOT null COMMENT '发布的内容(wang)',
    `ip` varchar(20) not null DEFAULT '127.0.0.1' COMMENT '发布者的IP'
    )ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- 比赛讨论表的修改

ALTER TABLE `contest_discuss` ADD `reply_time` INT NOT NULL COMMENT '最新回复时间' AFTER `ip`;


-- 增加qq 的openid 绑定字段
ALTER TABLE `users` ADD `qqid` VARCHAR(40) NULL DEFAULT NULL COMMENT 'QQ openid' AFTER `power`, ADD INDEX `qq_id` (`qqid`);

ALTER TABLE `contest` ADD `user_id` VARCHAR(48) NULL DEFAULT NULL COMMENT '创建比赛用户' AFTER `unmake_time`;


-- 呵呵 建主键
ALTER TABLE `contest_problem` ADD PRIMARY KEY(`contest_id`,`problem_id`); 

CREATE TABLE images(
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '自增主键',
    `user_id` varchar(48) NOT null COMMENT '图片所有者的用户ID',
    `in_time` int(11) NOT null COMMENT '上传图片的时间戳',
    `url` varchar(255) NOT null COMMENT '图片地址'
    )ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
