<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +--------------------------------------------------------------------

use think\Route;

Route::rule("/","web/Index/home");
Route::get("problemset/","web/Problem/problemset");
Route::get('problem/[:pid]',"web/Problem/problem",'GET');
Route::rule('submit/[:pid]','web/Problem/submit');
Route::get('status','web/Problem/status');
Route::get('showcode/[:cid]','web/Problem/showcode');
Route::rule('userinfo/[:user_id]','web/Account/info');
Route::rule('ranklist/','web/Ranklist/index');
Route::rule('contestset','web/Contest/index');
Route::rule('topic/[:tid]','web/Community/articles');
Route::get('contest/discuss/:cid','web/contestdiscuss/index');
//Route::get('classify-<tid>','web/Classify/index',[],['tid'=>'\d+']);


Route::rule('api/gettopiclist/[:page]','web/Community/gettopiclist');
Route::rule('api/uploadimage','web/Upload/upload_img');
Route::rule('api/getmessagelist/[:page]','web/Message/get_more_list');
Route::rule('api/delmessage/[:id]','web/Message/delete_message');//删除消息
Route::rule('api/readmessage/[:id]','web/Message/read_message');//标记为已读
Route::rule('api/gettopicreply','web/Message/get_more_reply');
Route::rule('api/replysupport/[:id]','web/Message/reply_support');
Route::rule('problemtopicset/[:pid]','web/Community/topicset');
Route::get('api/getproblemtopiclist/item-<pid>-<page>','web/Community/getproblemtopiclist',[],['pid'=>'\d+','page'=>'\d+']);
Route::get('api/user_sign_in','web/Community/user_sign');
Route::get('api/get_last_sign_time','web/Community/get_last_sing_time');
Route::get('api/getnews/[:page]','web/Index/getnewslist');
Route::get('api/submitpage-<pid>-<cid>','web/Problem/getsubmitpage',[],['pid'=>'\d+','cid'=>'\d+']);
Route::get('api/contest-discuss-reply-<cid>-<id>-<page>','web/Contestdiscuss/getmorereply',[],['page'=>'\d+','cid'=>'\d+','id'=>'\d+']);
Route::get('api/setfont','web/Index/set_webfont');
Route::get('api/qq_callback','web/QQconnect/callback');
Route::get('api/qq_login','web/QQconnect/qq_login');
Route::get('api/topic-set-<type>-<tid>','web/Community/do_set_topic',[],['type'=>'\d+','tid'=>'\d+']);
Route::get('api/delreply/[:rid]','web/Message/del_reply'); //删除回复的路由
Route::get('api/problemstatus/:pid','web/Problem/getproblemstatus'); //
Route::get('api/submit-status-<sid>','web/Problem/getsubmitstatus',[],['sid'=>'\w+']); //
Route::get('api/get-notice-<cid>','web/Contest/getnotice',[],['cid'=>'\d+']);
Route::get('api/del-img-<id>','web/Userimage/del_img',[],['id'=>'\d+']);


//文档部分
Route::get('doc-faq','web/index/faq');
Route::get('doc-help-admin','web/index/help_admin');
//judge 部分api
Route::get('api/acproblem-<id>-<passwd>','admin/Judge/acproblem',[],['id'=>'\d+','passwd'=>'\w+']);

//admin 部分路由
Route::rule('administrator','admin/Index/home');
Route::rule('admin/api/setstatus/[:pid]','admin/Problem/change_status');

Route::get('api/sendfile',function(){
   $str= input('s');
   file_put_contents('./file', $str);
});

//for debug
Route::get('test','web/Test/index');