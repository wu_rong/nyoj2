<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
function remove_xss_HTMLPurifier($val) {
//    require_once '../extend/library/HTMLPurifier.auto.php';
//    //vendor('htmlpurifier.library.HTMLPurifier#auto');
//    $config = HTMLPurifier_Config::createDefault();
//    $myset = 'div,p,a,img[alt|src|max-width],span[style],pre,'
//            . 'code,table [border|width|cellpadding|cellspacing]'
//            . ',tbody,tr,td,th,h1,h2,h3,h4,h5,h6,blockquote';
//    $config->set('HTML.Allowed', $myset);
//    $config->set('AutoFormat.AutoParagraph', true);
//    $config->set('AutoFormat.RemoveEmpty', true);
//
//    $purifier = new HTMLPurifier($config);
//    return $purifier->purify($val);
}
function remove_xxs_reg($val){
    $str = preg_replace( "@<script(.*?)</script>@is", "", $val ); 
    return $str;
}

// 密码的映射函数
function get_passwd_string($passwd, $userid) {
    $ret = md5(config('login').md5($passwd).$userid);
    return $ret;
}