<?php
namespace app\admin\controller;
use think\Controller;


class Contest extends AdminBase {

    //添加竞赛页面显示    权限:添加竞赛
    public function add() {
        if (!$this->have_power(4)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        return $this->fetch();
    }

    //竞赛列表页面显示    权限:编辑竞赛
    public function editor() {
        if (!$this->have_power(3)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        $list = db('contest')->field('contest_id,title,start_time,end_time,defunct')->order("start_time DESC")->paginate();
        $lists = $list->items();
        $this->assign('list', $lists);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    //竞赛添加逻辑实现    权限:添加竞赛
    public function add_opt() {
        if (!$this->have_power(4)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        $data['title'] = input('title');
        $data['start_time'] = input('start_time');
        $data['end_time'] = input('end_time');
        $data['defunct'] = input('defunct');
        $data['description'] = input('description');
        $data['private'] = input('private');
        $data['password'] = input('pws');
        $data['seal_time'] = input('seal_time', '');
        $data['unmake_time'] = input('unmake_time', '');
        $data['user_id']= $this->user_id;
        if ($data['seal_time'] == '') {
            $data['seal_time'] = $data['end_time'];
        }
        if ($data['unmake_time'] == ''||($data['unmake_time'] != ''&&$data['unmake_time']<$data['end_time'])) {
            $data['unmake_time'] = $data['end_time'];
        }

        $contest_id = db('contest')->insertGetId($data);
        $data = $_POST['problem'];
        foreach ($data as $key => $value) {
            $data[$key]['contest_id'] = $contest_id;
            if (empty($data[$key]['alias'])) {
                $data[$key]['alias'] = $data[$key]['problem_title'];
            }
        }
        // dump($data);return ;
        db('contest_problem')->insertAll($data);
        return json(['success' => 'success']);
    }

    //竞赛编辑逻辑实现    权限:编辑竞赛
    public function editor_opt() {
        if (!$this->have_power(3)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        $flag = input('flag');
        $id = input('id');
        if ($flag == '1') {//展示编辑比赛信息
            $data = db('contest')->where(['contest_id' => $id])->find();
            $data1 = db('contest_problem')->where('contest_id', $id)->order('num')->select();
            $this->assign('data', $data);
            $this->assign('data1', $data1);
            return $this->fetch();
        } else {//更新比赛信息
            $id= input('id');
            $data['title'] = input('title');
            $data['start_time'] = input('start_time');
            $data['end_time'] = input('end_time');
            $data['defunct'] = input('defunct');
            $data['description'] = input('description');
            $data['private'] = input('private');
            $data['password'] = input('pws');
            $data['seal_time'] = input('seal_time', '');
            $data['unmake_time'] = input('unmake_time', '');
            if ($data['seal_time'] == '') {
                $data['seal_time'] = $data['end_time'];
            }
            if ($data['unmake_time'] == '') {
                $data['unmake_time'] = $data['end_time'];
            }
            dump($data);
            db('contest')->where('contest_id', input('id'))->update($data);
            db('contest_problem')->where('contest_id', input('id'))->delete();
            db('contest_problem')->insertAll($_POST['problem']);
            return json(['success' => 'success']);
        }
    }

    private function create_contest_topic($user_id, $contest_id) {
        $data['title'] = '比赛-' . $contest_id . ' 的讨论';
        $data['status'] = 1;
        $data['support'] = 0;
        $data['type'] = -$contest_id;
        $data['user_id'] = $user_id;
        $data['content'] = '';
        $data['in_time'] = time();
        $data['ip'] = request()->ip();
        db('topic')->insert($data);
    }

    //竞赛删除    权限:编辑竞赛
    public function del() {
        if (!$this->have_power(3)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        db('contest')->delete(input('id'));
        $this->success('删除成功！', url('admin/contest/editor'));
    }

    //获取题目title
    public function get_title() {
        $val = input('id');
        $title = db('problem')->where(['problem_id' => $val])->value('title');
        $data = ['title' => $title];
        return json($data);
    }

    //搜索比赛    权限:编辑竞赛
    public function search() {
        if (!$this->have_power(3)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        $val = input('keyword');
        if ($val == '') {
            $list = db('contest')->where(1)->paginate();
        } else {
            $list = db('contest')->where('title', 'like', "%$val%")->whereOr(['contest_id' => $val])->paginate();
        }
        $lists = $list->items();
        $this->assign('list', $lists);
        $this->assign('keyword', $val);
        $this->assign('page', $list->render());
        return $this->fetch('contest/editor');
    }

}
