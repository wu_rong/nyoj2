<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\admin\controller;

/**
 * Description of Upload
 *
 * @author wurong
 */
use app\admin\controller\AdminBase;

class Upload extends AdminBase{
    public function upload_data(){
        ignore_user_abort();
        if(!$this->have_power(0)||!$this->have_power(1)){
            header('status:404 not foud');
//            return false;
            return json(['code'=>1,'msg'=>'error','error'=>true]);
        }
        $pid=input('pid',0,'intval');
        $id=input('id',"",'trim');
        $name=input('name',"",'trim');
        $size= input('size',"",'trim');
        $chunks= input('chunks',-1,'intval');
        $chunk= input('chunk',"",'intval');
        if($pid==0) {
            return json(['msg'=>'error']);
        }
        $file= request()->file('file');
        
        $base_path= config('judge.data_path').$pid.'/';
        if($chunks==-1){
            $file->move($base_path,$name);
            if ($name == 'spj.cpp'||$name == 'spj.c'||$name == 'spj.cc') {
                    $this->getspj($base_path, $name);
                }
                else {
                    chmod($base_path.$name, 0777);
                }
            return json(['msg' => 'success,not chunk']);
        }
        else {
            $file->move($base_path . $name . "_tmp", $chunk . '.tmp');
            $fsz='0';
            $fpsz=fopen($base_path.$name . "_tmp/size",'a+');
            flock($fpsz, LOCK_EX);
            fwrite($fpsz, '1');
            $fsz = filesize($base_path.$name . "_tmp/size");
            flock($fpsz, LOCK_UN);
            fclose($fpsz);
            if($fsz==$chunks){
                $this->mergeFile($pid,$name, $chunks);
                if ($name == 'spj.cpp'||$name == 'spj.c'||$name == 'spj.cc') {
                    $this->getspj($base_path, $name);
                }
                else {
                    chmod($base_path.$name, 0777);
                }
                return json(['msg' => 'success']);
            }
            return json(['msg' => 'pending']);
        }
    }
    private function mergeFile($pid,$filename,$totsize){
        $base= config('judge.data_path');
        $save=fopen($base.$pid."/".$filename,'wb+');
        for($i=0;$i<$totsize;$i++){
            $in= fopen($base.$pid.'/'.$filename."_tmp".'/'.$i.'.tmp', "rb");
            while (!feof($in)){
                fwrite($save, fread($in, 1204*1024));
            }
            fclose($in);
            unlink($base.$pid.'/'.$filename."_tmp".'/'.$i.'.tmp');
        }
        fclose($save);
        $this->delDirAndFile($base.$pid."/".$filename.'_tmp'.'/',true);
    }

    public function index(){
        return $this->fetch();
    }
    public function welcome(){
        echo 'hello world';
    }

    private function getspj($path,$name){ //路径末尾有 /
        system("g++ -o spj ".$path.$name." > ".$path.'spj.error');
        if(file_exists('./spj')){
            rename('./spj',$path.'spj');
            chmod($path."spj", 0777);
            if(file_exists($path.'spj.error')){
                unlink($path.'spj.error');
            }
        }
        
    }
}
