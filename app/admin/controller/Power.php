<?php
namespace app\admin\controller;
use think\Controller;


class Power extends AdminBase {
	//展示权限页面
	public function editor() {
		if(!$this->have_power(10)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$list = db('users')->where('power','like','%1%')->field('user_id,email,power,defunct,accesstime')->paginate();
		$lists = $list->items();
		$this->assign('list',$lists);
		$this->assign('keyword','');
		$this->assign('page',$list->render());
		return $this->fetch();
	}
	//展示编辑权限页面
	public function editor_opt() {
		if(!$this->have_power(10)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$id = input('id');
		$val = db('users')->where('user_id',$id)->value('power');
		if($val == NULL) {
			$this->assign('power','000000000000');
		} else {
			$this->assign('power',$val);
		}
		$this->assign('user_id',$id);
		return $this->fetch();
	}
	//保存权限逻辑
	public function save_opt() {
		if(!$this->have_power(10)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$id = input('user_id');
		$power = '';
		$power .= input('problem_edt') == 'on' ? '1' : '0';
		$power .= input('problem_add') == 'on' ? '1' : '0';
		$power .= input('problem_rejudge') == 'on' ? '1' : '0';
		$power .= input('contest_edt') == 'on' ? '1' : '0';
		$power .= input('contest_add') == 'on' ? '1' : '0';
		$power .= input('news_edt') == 'on' ? '1' : '0';
		$power .= input('radio_edt') == 'on' ? '1' : '0';
		$power .= input('news_add') == 'on' ? '1' : '0';
		$power .= input('pws_reset') == 'on' ? '1' : '0';
		$power .= input('make_id') == 'on' ? '1' : '0';
		$power .= input('power_edt') == 'on' ? '1' : '0';
                $power .= input('p11') == 'on' ? '1' : '0';
		db('users')->where('user_id',$id)->update(['power'=>$power]);
		$this->user_power = $power;
		if(!$this->have_power(10)) {
			$this->success('用户权限修改成功！',url('admin/index/welcome'));
		} else {
			$this->success('用户权限修改成功！',url('admin/power/editor'));
		}
		
	}
	//查找用户
	public function search() {
		if(!$this->have_power(10)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$val = input('keyword');
		if($val == '') {
			$list = db('users')->where(1)->paginate();
		} else {
			$list = db('users')->where('user_id',$val)->whereOr('email','like',"%$val%")->paginate();
		}
		$lists = $list->items();
		$this->assign('list',$lists);
		$this->assign('keyword',$val);
		$this->assign('page',$list->render());
		return $this->fetch('power/editor');
	}
}
