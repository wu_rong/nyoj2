<?php
namespace app\admin\controller;
use think\Controller;


class Pages extends AdminBase {
	//添加新闻展示页面    权限:添加新闻
	public function add_news() {
		if(!$this->have_power(7)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		return $this->fetch();
	}
	//添加新闻逻辑实现    权限:添加新闻
	public function add_opt() {
		if(!$this->have_power(7)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$data = $_POST;
		unset($data['file']);
		$data['time'] = date('Y-m-d H:i:s');
		$data['user_id'] = $_SESSION['user_id'];
		db('news')->insert($data);
		if(!$this->have_power(5)) {//没有编辑权限
			$this->success('添加成功！',url('admin/index/welcome'));
		} else {//有编辑权限
			$this->success('添加成功！',url('admin/pages/editor_news'));
		}
	}
	//编辑新闻页面展示    权限:编辑新闻
	public function editor_news() {
		if(!$this->have_power(5)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$list = db('news')->field('news_id,title,time,defunct,user_id,importance')->paginate();
		$lists = $list->items();
		$this->assign('list',$lists);
		$this->assign('keyword','');
		$this->assign('page',$list->render());
		return $this->fetch();
	}

	//编辑新闻逻辑    权限:编辑新闻
	public function editor_news_opt() {
		if(!$this->have_power(5)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$flag = input('flag');
		$id = input('id');
		if($flag=='1') {//展示新闻信息
			$data = db('news')->where(['news_id'=>$id])->find();
			$this->assign('data',$data);
			return $this->fetch();
		} else {//更新新闻信息
			$data = $_POST;
			$data['news_id'] = input('id');
			unset($data['file']);
			db('news')->where('news_id',input('id'))->update($data);
			$this->success('更新成功！',url('admin/pages/editor_news'));
		}
	}
	//编辑广播    权限:编辑广播
	public function editor_radio() {
		if(!$this->have_power(6)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$content = @file_get_contents('../data/file/radio.txt');
                //if(empty())
		$this->assign('data',$content);
		return $this->fetch();
	}
	//保存广播信息    权限:编辑广播
	public function editor_radio_opt() {
		if(!$this->have_power(6)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		file_put_contents('../data/file/radio.txt',input('content'));
		$this->success('编辑成功！');
	}
	//查找新闻    权限:编辑权限
	public function search() {
		if(!$this->have_power(5)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		$val = input('keyword');
		if($val == '') {
			$list = db('news')->where(1)->paginate();
		} else {
			$list = db('news')->where('title','like',"%$val%")->whereOr(['news_id'=>$val])->paginate();
		}
		$lists = $list->items();
		$this->assign('list',$lists);
		$this->assign('keyword',$val);
		$this->assign('page',$list->render());
		return $this->fetch('pages/editor_news');
	}
	//删除新闻    权限:编辑新闻
	public function del() {
		if(!$this->have_power(5)) {
			$this->error('对不起，您没有对应的权限！',url('admin/index/welcome'));
		}
		db('news')->delete(input('id'));
		$this->success('删除成功！',url('admin/pages/editor_news'));
	}
}
