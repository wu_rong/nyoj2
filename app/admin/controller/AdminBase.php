<?php

namespace app\admin\controller;

use think\Controller;

/**
 * 
 */
class AdminBase extends Controller {

    public $user_power = '00000000000';
    public $user_id="";
    function __construct(\think\Request $request = null) {
        parent::__construct($request);
        session_start();
        if (isset($_SESSION['user_id'])) {//判断是否登录 并加载权限
            $this->user_id=$_SESSION['user_id'];
            $power = db('users')->where('user_id', $this->user_id)->value('power');
            if ($power != NULL) {
                $this->user_power = $power;
            }
            $this->assign('user_power', $this->user_power);
        } else {//未登录则页面跳转
            $this->error('请先登录！', url('web/user/userlogin'));
        }
        $this->assign('user_id', $this->user_id);
    }

    //判断用户是否有具体权限 参数 权限id
    public function have_power($power_id) {
        if (!is_numeric($power_id)) {
            return FALSE;
        }
        if (isset($this->user_power[$power_id])&&$this->user_power[$power_id] == '1') {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    protected function delDirAndFile($path, $delDir = FALSE) {
        $handle = opendir($path);
        if ($handle) {
            while (false !== ( $item = readdir($handle) )) {
                if ($item != "." && $item != "..")
                    is_dir("$path/$item") ? delDirAndFile("$path/$item", $delDir) : unlink("$path/$item");
            }
            closedir($handle);
            if ($delDir)
                return rmdir($path);
        }else {
            if (file_exists($path)) {
                return unlink($path);
            } else {
                return FALSE;
            }
        }
    }
}
