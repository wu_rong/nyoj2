<?php

namespace app\admin\controller;

use think\Controller;

/**
 * 
 */
class Index extends AdminBase {

    public function home() {
        if ($this->user_power == 0) {
            $this->error('对不起，您没有访问后台的权限！', url('web/index/home'));
            return false;
        }
        return $this->fetch();
    }

    public function welcome() {
        if ($this->user_power == 0) {
            $this->error('对不起，您没有访问后台的权限！', url('web/index/home'));
            return false;
        }
        $this->assign('server_info', $this->getSystemInfo());
        return $this->fetch();
    }

    public function getSystemInfo() {
        $info = array(
            'os' => PHP_OS,
            'env' => $_SERVER["SERVER_SOFTWARE"]?$_SERVER["SERVER_SOFTWARE"]:"null",
            'ip'=>$_SERVER['SERVER_ADDR']?$_SERVER['SERVER_ADDR']:"",
            'up_max' => ini_get('upload_max_filesize'),
            'left' => round((disk_free_space(".") / (1024 * 1024)), 2) . 'M',
            'register_globals' => get_cfg_var("register_globals") == "1" ? "ON" : "OFF",
            'magic_quotes_gpc' => (1 === get_magic_quotes_gpc()) ? 'YES' : 'NO',
            'magic_quotes_runtime' => (1 === get_magic_quotes_runtime()) ? 'YES' : 'NO',
            'path'=>ROOT_PATH,
        );
        return $info;
    }
}
