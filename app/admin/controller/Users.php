<?php
namespace app\admin\controller;
use think\Controller;


class Users extends AdminBase {
    public function reset_pws() {
        if (!$this->have_power(8)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        $user_id = input('user_id');
        $password = input('password');
        if (!empty($user_id)) {
            $auth_string = get_passwd_string($password, $user_id);
            db('users')->where('user_id', $user_id)->setField('password', $auth_string);
            $this->success('修改成功！', url('admin/users/reset_pws'));
        }
        return $this->fetch();
    }

    public function make_id() {
        if (!$this->have_power(9)) {
            $this->error('对不起，您没有对应的权限！', url('admin/index/welcome'));
        }
        return $this->fetch();
    }

    public function sendmsg() {

        return $this->fetch();
    }
    public function do_sendmsg(){
        $user_id=input('userid',null,'trim');
        $content=input('content');
        if($this->have_power(8)){
            $info=db('users')->find($user_id);
            if(empty($info)){
                $this->error('该用户不存在');
            }
            $data['user_id']=$user_id;
            $data['content']=$content;
            $data['status']=1;
            $data['time']=time();
            db('message')->insert($data);
            $this->success('发送成功', url('admin/index/welcome'));
        }
        else {
            $this->error('没有权限');
        }
    }
}
