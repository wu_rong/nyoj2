<?php  


namespace app\web\controller;

use app\common\Base;

class About extends Base
{
    public function team() {
        return $this->fetch();
    }
}

