<?php

namespace app\web\controller;

use app\common\Base;

class Index extends Base{

    public function home(){
        $this->assign('web_title','首页');
        return $this->fetch();
    }
    public function getnewslist(){
        $page=input('page',1,'intval');
        $list=db('news')
            ->order('importance DESC,time DESC')->where(['defunct'=>'N'])
                ->limit(($page-1)*config('news.list_rows'),config('news.list_rows'))
                    ->select();
        $count=db('news')->where(['defunct'=>'N'])->count();
        $this->assign('list',$list);
        $res['html']= $this->fetch();
        $res['pages']= ceil($count/config('news.list_rows'));
        return json($res);
    }
    public function faq(){
        $this->assign('content', file_get_contents(APP_PATH.'../data/doc/faq.md'));
        return $this->fetch();
    }
    public function help_admin()
    {
        $content= file_get_contents('./../data/doc/admin.md');
        $this->assign('content',$content);
        return $this->fetch();
    }
}
