<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\web\controller;

use app\common\Base;

class Ranklist extends Base {
    
    public function index(){
        $condition= array();
        $userid= input('user_id','','trim');
        $nick= input('nick','','trim');
        if($userid!=''){
            $condition['user_id']=$userid;
        }
        if($nick!=''){
            $condition['nick']=['like',"%{$nick}%"];
        }
        $list=db('users')->where($condition)->order('solved DESC,submit ASC')->paginate(null,true);
        
        $nowpage=input('page',1,"intval");
        $startrank=(intval($nowpage)-1)*intval(config('paginate.list_rows'))+1;
        
        $this->assign('user_id',$userid);
        $this->assign('nick',$nick);
        $this->assign('list',$list);
        $this->assign('startrank',$startrank);
        $this->assign('tagval',2);
        return $this->fetch();
    }
}