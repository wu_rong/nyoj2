<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\web\controller;

use app\common\Base;

class Upload extends Base{
    public function upload_img(){
        $md = input('md');
        if ($md == 'true') {
            $ret['success'] = 0;
            if (!$this->is_login) {
                $ret['success'] = 0;
                $ret['message'] = '未登录,上传失败';
                return json($ret);
            }
            $ac_num = $this->get_ac_num();
            if ($ac_num < config('community.ac_problem_limit')) {
                $ret['success'] = 0;
                $ret['message'] = '上传失败,努力刷题.请保证ac题目数量大于' . config('community.ac_problem_limit');
                return json($ret);
            }
            $file = request()->file('editormd-image-file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            
            if ($file) {
                $info = $file->validate(['size'=>2097152,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public/upload/image/');
                if ($info) {
                    $url = $info->getSaveName();
                    $this->save_info($url);
                    $ret['success'] = 1;
                    $ret['message'] = '上传成功';
                    $ret['url'] = ['/upload/image/' . $url];
                } else {
                    $ret['success'] = 0;
                    $ret['message'] = $file->getError();
                }
            } else {
                $ret['success'] = 0;
                $ret['message'] = '其他错误,可能文件太大';
            }
            return json($ret);
        } else {
            $ret['errno'] = 0;
            if (!$this->is_login) {
                $ret['errno'] = 1;
                $ret['msg'] = '未登录,上传失败';
                return json($ret);
            }
            $ac_num = $this->get_ac_num();
            if ($ac_num < config('community.ac_problem_limit')) {
                $ret['errno'] = 1;
                $ret['msg'] = '上传失败,努力刷题.请保证ac题目数量大于' . config('community.ac_problem_limit');
                return json($ret);
            }
            $file = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下

            if ($file) {
                $info = $file->validate(['size'=>2097152,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public/upload/image/');
                if ($info) {
                    $url = $info->getSaveName();
                    $this->save_info($url);
                    $ret['msg'] = '上传成功';
                    $ret['data'] = ['/upload/image/' . $url];
                } else {
                    $ret['errno'] = 1;
                    $ret['msg'] = $file->getError();
                }
            } else {
                $ret['errno'] = 1;
                $ret['msg'] = '其他错误,可能文件太大';
            }
            return json($ret);
        }
    }

    private function save_info($url){
        $data['user_id']= $this->user_id;
        $data['in_time']=time();
        $data['url']=$url;
        db('images')->insert($data);
    }

    private function get_ac_num(){
        $res=db('users')->find($this->user_id);
        return $res?$res['solved']:0;
    }
}