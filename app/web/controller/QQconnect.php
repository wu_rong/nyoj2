<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\web\controller;

/**
 * Description of QQconnect
 *
 * @author wurong
 */
use app\common\Base;
use kuange\qqconnect\QC;

class QQconnect extends Base{
    
    public function callback(){
        $code= input('code',null);
        if($code==NULL){
            die('hello world');
        }
        $qc = new QC();
        $qc->qq_callback();    // access_token
        $openid=$qc->get_openid();     // openid
        $user_info= $this->get_user_info($openid);
        
        if($this->is_login){
            $this->bind($openid);
            $this->success_ui('绑定成功', url('/'));
        }
        if(empty($user_info)){
            $this->success_ui('该账户未绑定账号,请登录账号绑定QQ', url('/'));
        }
        else {
            $this->setUserID($user_info['user_id']);
            $this->success_ui('登录成功', url('/'));
        }
        
    }
    public function qq_login(){
        $qc = new QC();
        return redirect($qc->qq_login());
    }
    private function get_user_info($openid){
        return db('users')->where(['qqid'=>$openid])->find();
    }
    public function bind($openid){
        $ret=db('users')->where(['user_id'=> $this->user_id])->setField('qqid',$openid);
        if($ret==0){
            $this->success_ui('您已经绑定成功,请勿重复绑定','/');
        }
    }
}

