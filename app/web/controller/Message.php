<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace app\web\controller;

use app\common\Base;

class Message extends Base {
    public function messageset(){
        if(!$this->is_login){
            $this->error_ui('请先登录哦～');
        }
        $this->assign('nownav',2);
        return $this->fetch();
    }
    public function get_more_list(){
        if(!$this->is_login){
            return ;
        }
        $page= input('page',1,'intval');
        $condition['user_id']= $this->user_id;
        $list=db('message')->where($condition)->limit(($page-1)*config('community.pagenum'),
                config('community.pagenum'))->order('time DESC')->select();
        $this->assign('list',$list);
        $count=db('message')->where($condition)->count();
        //dump($list);
        $ret['pages']=ceil($count /config('community.pagenum'));
        $ret['html']= $this->fetch();
        //dump($ret);
        return json($ret);
    }

    public function read_message(){
        $id=input('id',0,'intval');
        $ret['code']=0;
        if(!$this->is_login){
            $ret['code']=1;
            $ret['msg']='请先登陆';
        }
        else {
            $res=db('message')->where('id='.$id)->setField('status', 0);
            if($res==FALSE){
                $ret['code']=1;
                $ret['msg']='不见了';
            }
            else {
                $ret['msg']='已读成功';
            }
        }
        return json($ret);
    }
    public function do_delete(){
        if(!$this->is_login){
            $this->error_ui('请先登陆');
            return;
        }
        else {
            $condition['user_id']= $this->user_id;
            db('message')->where($condition)->delete();
            $this->success_ui('清空成功',url('web/Message/messageset'));
            return ;
        }
    }
    
    private function do_del_msg($id,$userid){
        $condition['id']=$id;
        $condition['user_id']=$userid;
        return db('message')->where($condition)->delete();
    }

    public function delete_message(){
        $id= input('id',-1,'intval');
        $ret['code']=0;
        if($id==-1) {
            return ;
        }
        if(!$this->is_login){
            $ret['code']=1;
            $ret['msg']='未登录';
        }
        else {
            $res=$this->do_del_msg($id, $this->user_id);
            if($res==1){
                $ret['msg']='删除成功';
            }
            else {
                $ret['code']=1;
                $ret['msg']='该消息已经不见';
            }
        }
        return json($ret);
    }
    private function do_reply_message($userlist,$topic_id,$content){
        foreach ($userlist as $uid){
            $this->assign('user_id', $this->user_id);
            $this->assign('topic_id',$topic_id);
            $info = db('topic')->find($topic_id);
            if(empty($info)) 
                return;
            
            $this->assign('title', $info['title']);
            $this->assign('content', $content);

            $data['content']= $this->fetch('do_comment_reply');
            $data['user_id']=$uid;
            $data['time']=time();
            $data['status']=1;
            db('message')->insert($data);
        }
    }
    private function get_user($html)
    {
        $a=array();
        $reg='/@([^:]+)[:：]/U';
        preg_match_all($reg, $html,$a);
        $ret=array();
        foreach ($a[1] as $k=>$v) {
            $ret[$k]=$v;
        }
        return $ret;
    }
    
    private function save_reply($content,$topic_id){
        $data['user_id']= $this->user_id;
        $data['in_time']=time();
        $data['content']=$content;
        $data['topic_id']=$topic_id;
        $data['status']=1;
        $data['ip']= request()->ip();
        $data['support']=0;
        db('reply')->insert($data);
    }

    private function do_comment_reply($topic_id, $content){
        $res=db('topic')->find($topic_id);
        $data['user_id']=$res['user_id'];
        if($data['user_id']== $this->user_id) {
            return ;
        }
        $data['time']=time();
        $this->assign('user_id', $this->user_id);
        $this->assign('topic_id',$topic_id);
        $info = db('topic')->find($topic_id);
        if(empty($info)) 
                return;
            
        $this->assign('title', $info['title']);
        $this->assign('content', $content);
        $data['content']= $this->fetch('do_comment_reply');
        $data['status']=1;
        db('message')->insert($data);
    }

    public function reply(){
        $topic_id= input('topic_id',-1,'intval');
        $content= $this->remove_xss(input('content','','trim'));
        $ret['code']=0;
        if(!$this->is_login){
            $ret['code']=1;
            $ret['msg']='未登录';
            return json($ret);
        }
        if($content==""){
            $ret['code']=1;
            file_put_contents('test.txt', $content);
            $ret['msg']='不可以回复空内容';
        }
        else {
            $ret['code']=0;
            $userlist= $this->get_user($content);
            $this->save_reply($content, $topic_id);
            $this->do_reply_message($userlist, $topic_id, $content);
            $this->do_comment_reply($topic_id, $content);
            $ret['msg']='success';
        }
        return json($ret);
    }
    private function get_reply_info($rid){
        $condition['rid']=$rid;
        $res=db('reply')->find($rid);
        if(empty($res)){
            return FALSE;
        }
        else {
            return $res;
        }
    }
    private function is_your_reply($rid,$uid){
        $info=$this->get_reply_info($rid);
        if($info!=FALSE&&$info['user_id']==$uid) {
            return TRUE;
        }
        else {
            return false;
        }
    }

    public function get_more_reply(){
        $page= input('page',1,'intval');
        $tid= input('tid',-1,'intval');
        $condition['topic_id']=$tid;
        $list=db('reply')->where($condition)->limit(($page-1)*config('community.pagenum'),
                config('community.pagenum'))->order('support DESC,in_time DESC')->select();
        foreach ($list as $key=>$value){
            $list[$key]['head']= $this->GetUserHead($value['user_id']);
            $list[$key]['is_your']=$this->is_your_reply($list[$key]['rid'], $this->user_id);
        }
        $this->assign('is_admin', $this->is_admin);
        $this->assign('list',$list);
        $count=db('reply')->where($condition)->count();
        //dump($list);
        $ret['pages']=ceil($count /config('community.pagenum'));
        $ret['html']= $this->fetch();
        return json($ret);        
    }
    private function get_coin_num($userid){
        $res=db('users')->find($userid);
        return intval($res['ojcoin']);
    }
    private function reduce_coin($userid){
        $condition['user_id']=$userid;
        db('users')->where($condition)->setDec('ojcoin');
    }
    private function do_reply_support($rid){
        $condition['rid']=$rid;
        return db('reply')->where($condition)->setInc('support');
    }
    private function judge_reply_exist($id){
        $res=db('reply')->find($id);
        if(empty($res)){
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    public function reply_support(){
        $rid=input('id');
        //dump($rid);
        $ret['code']=0;
        $ret['msg']='点赞成功';
        if(!$this->is_login){
            $ret['code']=1;
            $ret['msg']='未登录';
            return json($ret);
        }
        $coin_num= $this->get_coin_num($this->user_id);
        if($coin_num<1) {
            $ret['code']=1;
            $ret['msg']='OJ币不足';
            return json($ret);
        }
        if($this->judge_reply_exist($rid)==FALSE){
            $ret['code']=1;
            $ret['msg']='该回复，不存在';
            return json($ret);
        }
        $this->reduce_coin($this->user_id);
        $this->do_reply_support($rid);
        return json($ret);
    }
    public function del_reply(){
        $rid=input('rid',null,'intval');
        if($rid==NULL){
            $ret['code']=1;
            $ret['msg']='没有要删除的?';
            return json($ret);
        }
        if(!$this->is_login){
            $ret['code']=1;
            $ret['msg']='未登录';
            return json($ret);
        }
        $replyinfo= $this->get_reply_info($rid);
        if($replyinfo==false){
            $ret['code']=1;
            $ret['msg']='该回复早已不见';
            return json($ret);
            return ;
        }
        if($replyinfo['user_id']== $this->user_id||$this->have_power(9)){
            db('reply')->delete($rid);
            $ret['code']=0;
            $ret['msg']='删除成功';
            if($replyinfo['user_id'] != $this->user_id){
                $this->sendmessage($replyinfo['user_id'], "管理员删除了你在"
                        . "<a href='".url('web/Community/articles',['tid'=>$replyinfo['topic_id']])."' ><cite>这篇文章"
                        . "</cite></a>的回复");
            }
            return json($ret);
        }
        else {
            $ret['code']=1;
            $ret['msg']='没有权限哦~';
            return json($ret);
        }
    }
} 