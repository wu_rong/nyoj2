<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\web\controller;

/**
 * Description of Cmd
 *
 * @author wurong
 */
use app\common\Base;
class Cmd extends Base{
    public function __construct(\think\Request $request = null) {
        parent::__construct($request);
        if(!$this->is_login){
            $this->error_ui('请先登录');
        }
        if(!$this->have_power(10)){
            $this->error_ui('对不起，您没有权限');
        }
        header("Content-type: text/html; charset=utf-8");
    }
    private function have($pid,$tid){
        $con['problem_id']=$pid;
        $con['tag_id']=$tid;
        $res=db('problem_tag_match')->where($con)->find();
        if(empty($res)){
            return false;
        }
        else {
            return true;
        }
    }
    private function add_tag($pid,$tid){
        $con['problem_id']=$pid;
        $con['tag_id']=$tid;
        db('problem_tag_match')->insert($con);
    }

    public function update_tag(){
        $d= require_once '../install/tag';
        $rename[1]=0;
        $rename[2]=13;
        $rename[3]=10;
        $rename[4]=7;
        $rename[5]=2;
        $rename[6]=0;
        $rename[7]=0;
        $rename[8]=1;
        $rename[9]=6;
        $rename[10]=3;
        $rename[11]=3;
        foreach ($d as $v){
            if($this->have($v[1], $rename[$v[0]])==FALSE){
                $this->add_tag($v[1], $rename[$v[0]]);
                echo '添加成功';
                dump($v);
            }
            else 
            {
                echo "已经存在pid={$v[1]} tagid={$rename[$v[0]]}"."<br />";
            }
        }
    }
}
