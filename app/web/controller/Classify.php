<?php
namespace app\web\controller;
use app\common\Base;
/**
* 
*/
class Classify extends Base
{
    public function get_tag_html() {
        $tag1 = db('problem_tag')->where(['tag_parent_id'=>NULL])->order('tag_id')->select();
        echo '<div class="layui-collapse" lay-accordion="">' ;
        foreach ($tag1 as $key1 => $value1) {
            echo '<div class="layui-colla-item">';
            echo '<h2 class="layui-colla-title"><a href ="'. url('web/classify/index',['tid'=>$value1['tag_id']]) . '" class="href_color">' .$value1['tag_name']. '</a></h2>';
            $tag2_id =db('problem_tag')->where(['tag_parent_id'=>$value1['tag_id']])->order('tag_id')->select();
            echo '<div class="layui-colla-content">' ;
            echo '<div class="layui-collapse" lay-accordion="">' ;
            foreach ($tag2_id as $key2 => $value2) {
                echo '<div class="layui-colla-item">' ;
                echo '<h2 class="layui-colla-title"><a href ="'. url('web/classify/index',['tid'=>$value2['tag_id']]) . '" class="href_color">' .$value2['tag_name']. '</a></h2>';
                $tag3_id = db('problem_tag')->where(['tag_parent_id'=>$value2['tag_id']])->select();
                if(!empty($tag3_id)) {
                    echo '<div class="layui-colla-content">' ;
                    echo '<div class="layui-collapse" lay-accordion="">' ;
                    foreach ($tag3_id as $key3 => $value3) {
                        echo '<div class="layui-colla-item">' ;
                        echo '<h2 class="layui-colla-title"><a href ="'. url('web/classify/index',['tid'=>$value3['tag_id']]) . '" class="href_color">' .$value3['tag_name']. '</a></h2>';
                        echo '<div class="layui-colla-content">';  //如果不想显示没有了，删除这句话
                        echo '<p>没有了</p>'; //如果不想显示没有了，删除这句话
                        echo '</div>'; //如果不想显示没有了，删除这句话
                        echo '</div>';
                    }
                    echo '</div>';
                    echo '</div>';
                }   
                else { //如果不想显示没有了，删除这句话
                    echo '<div class="layui-colla-content">'; //如果不想显示没有了，删除这句话
                    echo '<p>没有了</p>';//如果不想显示没有了，删除这句话v
                    echo '</div>';//如果不想显示没有了，删除这句话//如果不想显示没有了，删除这句话
                }//如果不想显示没有了，删除这句话//如果不想显示没有了，删除这句话
                echo '</div>';
            }
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
        echo '</div>';
    }
    public function getstatus($user_id,$pid){
        if(!$this->is_login){
            return 2;
        }
        $condition['user_id']=$user_id;
        $condition['problem_id']=$pid;
        $condition['result']=4;
        $ac_res=db('solution')->where($condition)->find();
        if(!empty($ac_res)){
            return 0;
        } else {
            $condition2['user_id']=$user_id;
            $condition2['problem_id']=$pid;
            $at_res=db('solution')->where($condition2)->find();
            if(!empty($at_res)){
                return 1;
            } else {
                return 2;
            }
        }
    }
    public function index() {
        $tid = input('tid');
        $current_tag_name = db('problem_tag')->field('tag_name')->where(['tag_id'=>$tid])->select();
        $sql=db('problem_tag_match')->field('problem_id')->where(['tag_id'=>$tid])->select(false);
        $list = db('problem')->where(' problem_id in ' . "($sql)")->order('problem_id')->paginate(30);
        $lists=$list->items();
        $page = $list ->render();
        /*标签start*/
        $tag_lists = array();
        foreach ($lists as $key=>$val){
            // dump($val);
            $lists[$key]['result'] =$this->getstatus($this->user_id,$val['problem_id']);
            $tag_id = db('problem_tag_match')->where(['problem_id'=>$val['problem_id']])->field('tag_id')->select();
            foreach ($tag_id as $key => $value) {
                $tag_name = db('problem_tag')->where(['tag_id'=>$value['tag_id']])->field('tag_id,tag_name')->select();
                if(isset($tag_name[0]))
                                                $tag_lists[$val['problem_id']][]=$tag_name[0];
            }
        }
        // dump($tag_lists);
        $this->assign('tag_lists',$tag_lists);
        /*标签over*/
        $this->assign('tag_name',$current_tag_name[0]['tag_name']);
        $this->assign('list' , $lists);
        $this->assign('page',$page);
        return $this->fetch();
    }
}