<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\web\controller;

use app\common\Base;

class Test extends Base{
    function __construct(\think\Request $request = null){
        parent::__construct($request);
        if(config('app_debug')!='true'){
            exit();
        }
    }
    public function index(){
        return $this->fetch();
    }
}