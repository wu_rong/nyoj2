<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\web\controller;

/**
 * Description of UserImage
 *
 * @author wurong
 */
use app\common\Base;
class Userimage extends Base{
    public function __construct() {
        parent::__construct();
        $this->assign('nownav',3);
    }
    
    public function index(){
        if(!$this->is_login){
            $this->error_ui('请先登录');
        }
        
        $list=db('images')->where('user_id', $this->user_id)->order('in_time DESC')->paginate();
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function del_img(){
        $id= input('id'.null);
        $ret['code']=1;
        if($id==NULL){
            $ret['msg']='ID为空';
            return json(['code']);
        }
        if(!$this->is_login){
            $ret['msg']='请先登录';
            return json($ret);
        }
        $info= db('images')->find($id);
        if($info['user_id']== $this->user_id||($this->have_power(9)&&$this->have_power(10))){
            $path=APP_PATH.'../public/upload/image/'.$info['url'];
            if(file_exists($path)){
                unlink($path);
            }
            db('images')->delete($id);
            $ret['code']=0;
            $ret['msg']='删除成功';
            return json($ret);
        }
    }
}
