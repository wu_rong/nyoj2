<?php

namespace app\web\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;

/**
 * Description of Hello
 *
 * @author wurong
 */
class Hello extends Command{
    protected function configure()
    {
        $this->setName('hello')->setDescription('Here is the remark ');
    }

    protected function execute(Input $input, Output $output)
    {
        $output->choice($input, '请选择', ['1','2','3']);
    }
}
