### 上传题目

上传题目请认真填写表单中的内容，请注意单位。

* 时间是秒，不是毫秒
* 内存是 MB
* 难度 1-10



### 测试数据

上传题目后，就是上传测试数据。本OJ测试数据采用这种命名格式

* 输入数据为 *.in
* 输出数据为 *.out

每一组测试数据文件名一样，不同的是文件扩展名。



### 特判问题（special judge）

如果本题目是特判，请在添加问题（或者 修改问题）中将 特判: 改为 [是]。

然后上传特判的代码。特判的代码请命名为 `spj.cpp` 或者 `spj.c` 再或者 `spj.cc` 。

上传成功后若存在 spj 文件，意思为您的代码能够变异成功。

如果没有代码您的代码不能够编译成功，可以下载文件`spj.error`进行查错。



### 特判代码要求

下面是精度误差的特判代码，误差再 0.0001 范围内算正确的特判代码。

返回值为 0 代表 答案通过，否则答案错误。

主函数的三个参数在下面注释中。分别代表`测试输入`，`测试输出`，`用户输出`。

文件输入推荐 fscanf，等系列函数。详细细节参考如下代码。

``` cpp
#include <stdio.h>
#include <math.h>
const double eps = 1e-4;
int main(int argc, char *args[]) //主函数
{
    FILE *f_in = fopen(args[1],"r");   //测试输入
    FILE *f_out = fopen(args[2],"r");  //测试输出
    FILE *f_user = fopen(args[3],"r"); //用户输出
    int ret = 0;                       //返回值
    int T;
    double a, x;
    fscanf(f_in,"% d", &T); //从输入中读取数据组数T
    while (T--)
    {
        fscanf(f_out,"% lf", &a);
        fscanf(f_user,"% lf", &x);
        if (fabs(a - x) > eps)
            ret = 1; //Wrong Answer
    }
    fclose(f_in);
    fclose(f_out);
    fclose(f_user);
    return ret;
}
```



