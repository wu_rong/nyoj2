## Problem 控制器

### getsubmitstatus 方法

参数: sid(solution_id)

返回值:返回一个json数组

``` json
code:0,
msg:'消息',
data:{
  ... //这里是sulotion表的信息
}
```

#### 函数说明: 

获取sulotion的信息,比赛中的只能获取自己的信息。获取代码信息，提交结果。可以多条信息一起查询，返回json数组。

#### 路由规则: 

``` php
Route::get('api/submit-status-<sid>','web/Problem/getsubmitstatus',[],['sid'=>'\w+']); 
```

#### 请求参数：请求时，多个提交ID建议使用 下划线隔开。

下面是例子：

``` htt
http://acm.nyist.edu.cn/api/submit-status-5   // 单个提交ID的请求
http://acm.nyist.edu.cn/api/submit-status-6   //  单个提交ID的请求
http://acm.nyist.edu.cn/api/submit-status-1_2_3_4_5_6_7 // 多个提交ID的请求，每个数字之间建议使用下划线隔开。
```

####　返回值说明

``` json
[
  { 
    "solution_id": 16877, 　　　　　　　　　	// 提交ID
    "problem_id": 2383, 				  // 问题ID
    "user_id": "1506915009", 			  // 用户ID
    "time": 0, 							// 代码耗时 ， 单位 ms
    "memory": 0,               //  代码所使用的内存     单位 KB
    "in_date": "2018-03-25 20:07:07",     // 提交时间 datetime类型
    "result": 2,                          // 提交结果的代码
    "result_zh": "编译中..",              // 提交结果的中文
    "language": 3,                       //提交语言代码
    "code_length": 0,                     // 代码长度
    "code": 0,                            // 返回的状态码 0 为成功，非0失败
    "msg": "获取状态成功"                   // 提示消息
  },
  {
    "code": 1,
    "msg": "没有权限"
  }
]
```



#### 备注:

 路由后请求路径  sid 为必填

``` shell
/api/submit-status-<sid>
```


