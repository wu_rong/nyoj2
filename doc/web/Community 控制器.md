## Community 控制器

* 说明: 继承与 Base (命名空间:app\\web\\controller\\Base)
* 主要作用: 社区的一些操作,反正就是社区的控制器。

### 方法

#### is_your_topic

参数：tid（topic_id字段），uid（用户ID）

说明：判断是不是该用户的文章。

返回值：true or false （true代表是该用户的文章）



#### do_set_topic

参数：tid（要操作的文章id），type（操作说明）

说明：对该文章进行操作（删除，隐藏）

返回值：json格式

|code 

备注：type说明，见下表

| type | 表示的操作 |
| ---- | ----- |
| 0    | 删除文章  |
| 1    | 隐藏文章  |
| 2    | 显示文章  |



``` json
{
  code:1,
  msg:'提示消息'
}
```



