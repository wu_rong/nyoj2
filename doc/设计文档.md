

## 数据库设计

#### message 表

说明: 这张表主要用于给用户发送消息,记录事件。告知用户。讨论区的回复，以及其他的系统消息。

| 字段名     | 数据类型        | 注释      |
| ------- | ----------- | ------- |
| id      | int         | 消息的id   |
| user_id | varchar(48) | 关联的用户id |
| content | TEXT        | 消息的内容   |
| status  | int 4       | 表示该消息状态 |
|         |             |         |

status 状态：默认为0 表示用户未读。

#### reply 表

| 字段名  | 数据类型   | 注释      |
| ---- | ------ | ------- |
| rid  | int 11 | 回复消息的id |
|      |        |         |
|      |        |         |
|      |        |         |
|      |        |         |



#### topic 表

| 字段名      | 数据类型         | 注释              | 默认        | 是否可为空 |
| -------- | ------------ | --------------- | --------- | :---- |
| topic_id | int(11)      | 主键              |           | no    |
| user_id  | varchar(48)  | 发表文章的用户ID       |           | no    |
| in_time  | int(11)      | 发表的时间(时间戳)      | time()    | no    |
| title    | varchar(255) | 文章的标题           |           | no    |
| content  | MEDIUMTEXT   | 文章内容            |           | no    |
| ip       | varchar(20)  | 发布者的IP          | 127.0.0.1 | no    |
| support  | int(11)      | 发布者支持数量         | 0         | no    |
| type     | int(11)      | 文章类型，问题、比赛、普通讨论 | 0         | no    |



#### problem 表



#### users 表

| 字段名     | 数据类型        | 注释    |
| ------- | ----------- | ----- |
| user_id | varchar(48) | 用户的ID |
|         |             |       |
|         |             |       |
|         |             |       |
|         |             |       |
|         |             |       |


#### contest_discuss 表

| 字段名        | 数据类型        | 注释                      |
| :--------- | ----------- | ----------------------- |
| id         | int(11)     | 自增                      |
| pre        | int(11)     | 父亲节点, -1 代表根节点，-2为管理员通知 |
| cid        | int(11)     | 比赛ID                    |
| num        | int(11)     | 比赛的第几题 -1 代表public讨论    |
| user_id    | varchar(48) | 发布者的用户ID                |
| in_time    | int(11)     | 发布的时间戳                  |
| content    | text        | 发布的内容(wang)             |
| ip         | varchar(20) | 发布者的IP                  |
| reply_time | int(11)     | 最新回复时间                  |

##### 





## 后台管理权限设置

权限存储在 users 表中的字段（power）中,盼盼最后决定用一个01字符串代表某个权限的有无。具体请看下表



| 对应为0/1 | 表示拥有的权限 （1表示拥有该权限）            |
| ------ | ----------------------------- |
| 0      | 编辑问题                          |
| 1      | 添加问题                          |
| 2      | 重判问题                          |
| 3      | 编辑比赛                          |
| 4      | 添加竞赛                          |
| 5      | 编辑新闻（新闻在首页展示）                 |
| 6      | 编辑广播（广播在公告位置展示）               |
| 7      | 添加新闻                          |
| 8      | 用户管理（重置密码，能够查看所有用户代码，生成账号。。。） |
| 9      | 社区管理（管理社区的帖子）                 |
| 10     | 编辑权限（拥有此权限就是超牛逼）              |
| 11     | 编辑分类                          |
|        |                               |
|        |                               |

##### 说明

###### 权限0:

* 编辑问题的权限
* 编辑问题分类的权限



###### 权限8：

* 重置用户密码
* 查看用户代码(包括比赛)
* 查看用户的邮箱


__对于删除操作，比赛额外拥有10 权限。才允许; 测试数据浏览下载上传需要有1权限__




## OJ支持语言对应表

| 字段值  | 含义             |
| ---- | -------------- |
| 0    | C              |
| 1    | C++            |
| 2    | Pascal         |
| 3    | Java           |
| 4    | Ruby           |
| 5    | Bash           |
| 6    | Python         |
| 7    | PHP            |
| 8    | Perl           |
| 9    | C#             |
| 10   | Obj-C          |
| 11   | FreeBasic      |
| 12   | Schema         |
| 13   | Clang          |
| 14   | Clang++        |
| 15   | Lua            |
| 16   | Other Language |
|      |                |







## 判题结果对应表



| 字段值  | 含义    |
| ---- | ----- |
| 0    | 提交中.. |
| 1    | 等待重判  |
| 2    | 编译中.. |
| 3    | 判题中.. |
| 4    | 答案正确  |
| 5    | 格式错误  |
| 6    | 答案错误  |
| 7    | 时间超限  |
| 8    | 内存超限  |
| 9    | 输出超限  |
| 10   | 运行时错误 |
| 11   | 编译失败  |
| 12   | 编译完成  |
| 13   | 测试运行  |
|      |       |
|      |       |
|      |       |





## 项目文件结构设计



```
nyoj-v2                                  	项目文件
├── app										应用目录
│   ├── admin								admin（管理员后台管理模块）
│   │   ├── controller						控制器目录
│   │   │   ├── AdminBase.php				后台管理Base类
│   │   │   ├── Contest.php					比赛类（编辑比赛等相关）
│   │   │   ├── Index.php					欢迎界面
│   │   │   ├── Pages.php					新闻消息的控制器
│   │   │   ├── Power.php					权限管理相关
│   │   │   ├── Problem.php					问题管理相关
│   │   │   ├── Upload.php					上传（测试数据）
│   │   │   └── Users.php					用户管理
│   │   ├── model							模型
│   │   │   └── Problem.php				
│   │   └── view							视图（前台部分）
│   │       ├── common						公共部分（管理的iframe）
│   │       ├── contest						比赛部分
│   │       ├── index						欢迎界面部分
│   │       ├── pages						消息相关前台
│   │       ├── power						权限
│   │       ├── problem						问题相关
│   │       ├── public						页面公共
│   │       │   ├── end.html				页脚（footer）
│   │       │   ├── head.html				网页头部
│   │       │   ├── kind_editor.html		。。。
│   │       │   ├── msg.html				消息模板
│   │       │   ├── nav-2.html				二级导航
│   │       │   ├── nav.html				一级导航
│   │       │   ├── side-bar.html			边栏
│   │       │   └── side-nav.html			边栏导航
│   │       ├── upload						上传的前台页面
│   │       └── users						用户相关的
│   ├── command.php							命令行工具配置文件
│   ├── common								应用公共（函数）文件
│   │   └── Base.php						web模块的基类
│   ├── common.php							应用公共（函数）文件
│   ├── config.php							应用（公共）配置文件
│   ├── database.php						数据库配置文件
│   ├── extra								
│   │   ├── queue.php
│   │   └── rank_user.php
│   ├── route.php							路由配置文件
│   ├── tags.php							应用行为扩展定义文件
│   └── web									web模块（OJ用户的模块）
│       ├── command							命令行工具（实现）
│       │   └── Test.php					
│       ├── controller						控制器目录
│       │   ├── About.php					关于我们
│       │   ├── Account.php					账号相关，类似 User 类
│       │   ├── Classify.php				分类相关
│       │   ├── Cmd.php						管理员web访问的命令
│       │   ├── Community.php				社区讨论区
│       │   ├── Contestdiscuss.php			比赛的讨论
│       │   ├── Contest.php					比赛相关
│       │   ├── Index.php					首页
│       │   ├── Message.php					通知消息
│       │   ├── Problem.php					问题相关
│       │   ├── QQconnect.php				QQ互联的类
│       │   ├── Ranklist.php				比赛的榜
│       │   ├── Upload.php					上传图片
│       │   ├── Userimage.php				用户管理上传图片
│       │   └── User.php					用户账号类，类似 Account
│       ├── model
│       │   ├── Topic.php
│       │   └── Users.php
│       └── view
│           ├── mobile
│           └── pc
├── data 									存放数据
│   ├── index.html
│   ├── recycle
│   │   ├── images
│   │   └── topic
│   └── upload
├── extend									扩展类库目录（可定义）
│   └── kuange								kuange整理的QQconnect
│       └── qqconnect						QQconnect
├── install									OJ的判题核心，以及数据库sql
│   ├── judge								判题核心（hustoj）
│   ├── oj.sql								安装sql语句
│   └── update.sql							修改表的sql语句
├── public									web根目录
│   ├── favicon.ico							网站图标 ico
│   ├── index.php							入口文件
│   ├── router.php							路由文件
│   ├── static								静态文件（前台框架和项目）
│   └── upload								用户上传的（图片）
│       ├── data							数据
│       ├── image							网站自带图片
│       ├── old-img							老版本OJ的图片
│       │   └── attached					由于（kind-editor）所建文件目录
│       └── users							用户的图片
│           └── head						头像文件目录（有一张默认 default.png）
├── think									think命令入口
├── thinkphp								thinkphp框架的核心
└── vendor									composer安装的php-lib

```





