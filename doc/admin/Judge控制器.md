## 说明

该控制器主要负责judge通过 wget 传过来的触发信号。web做出对应的响应。



## 配置相关

配置文件：

```
'judge'=>[
        'passwd'=>'××××××',   //judge的口令
    ],
```



## 方法函数说明

### acproblem 函数

#### api调用

``` /api/acproblem-<id>-<passwd>```

* id 表示 solution_id 

* passwd 表示判题机的口令，口令配置文件 

* > web 的口令在       /app/config.php        judge.passwd
  >
  > judge 的口令在    /home/judge/etc/judge.cong      judge_passwd

####　说明

当判题机判断出某一份代码结果为AC时。触发这个函数使得用户的ojcoin加题目难度对应的OJcoin。



#### 返回值

 。。。。。。

#### 日志

日志在 runtime/Log/

